?xml version="1.0" encoding="UTF-8" standalone="no"?><tns:metadata xmlns:tns="http://www.tomtom.com/ns/cpt/metadata/2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" generationDate="2021-05-27T06:36:14" schemaName="cpt-metadata-2.0.0" schemaVersion="2.0.0" type="TomTom_Content_Group_Metadata" xsi:schemaLocation="http://www.tomtom.com/ns/cpt/metadata/2 cpt-metadata-2.0.0.xsd">
<tns:group identifier="CG-TT-PSA.P1CMS.UP-MAP.JAPAN.NDS-3.0.0" releaseDate="2021-05-27T06:36:14" supplier="TomTom" targetCustomer="PSA" targetProduct="P1CMS" targetQualifier="Update_Production" version="3.0.0" xsi:type="tns:ContentMapGroupType">
	<tns:groupTotalSizeInBytes>12816993074</tns:groupTotalSizeInBytes>
	<tns:groupStorageSizeInBytes>12816994304</tns:groupStorageSizeInBytes>
    <tns:item identifier="CI-TT-PSA.P1CMS.UP-MAP.JAPAN.NDS-3.0.0" releaseDate="2021-05-27T06:36:12" supplier="TomTom" targetCustomer="PSA" targetProduct="P1CMS" targetQualifier="Update_Production" version="3.0.0" xsi:type="tns:ContentMapNDSItemType">
    	<tns:itemTotalSizeInBytes>12816990208</tns:itemTotalSizeInBytes>
		<tns:itemStorageSizeInBytes>12816990208</tns:itemStorageSizeInBytes>
		<tns:region>Japan</tns:region>
		<tns:format>NDS</tns:format>
		<tns:geoCoverage>JPN</tns:geoCoverage>
        <tns:releaseType precondition="CI-TT-PSA.P1CMS.UP-MAP.JAPAN.NDS-1.0.0 CI-TT-PSA.P1CMS.UP-MAP.JAPAN.NDS-2.0.0" predecessor="CI-TT-PSA.P1CMS.UP-MAP.JAPAN.NDS-2.0.0" xsi:type="tns:MapItemUpdateReleaseType"/>
        <tns:navigationSoftwareCompatibilityList>CI-TT-PSA.P1CMS.P-NAVSW-20.20.1 CI-TT-PSA.P1CMS.P-NAVSW-20.20.4 CI-TT-PSA.P1CMS.P-NAVSW-20.22.1 CI-TT-PSA.P1CMS.P-NAVSW-20.22.4 CI-TT-PSA.P1CMS.P-NAVSW-20.24.1 CI-TT-PSA.P1CMS.P-NAVSW-20.26.1 CI-TT-PSA.P1CMS.P-NAVSW-20.26.4 CI-TT-PSA.P1CMS.P-NAVSW-20.28.3 CI-TT-PSA.P1CMS.P-NAVSW-20.29.4 CI-TT-PSA.P1CMS.P-NAVSW-20.30.2 CI-TT-PSA.P1CMS.P-NAVSW-20.32.2 CI-TT-PSA.P1CMS.P-NAVSW-20.34.2 CI-TT-PSA.P1CMS.P-NAVSW-20.37.3 CI-TT-PSA.P1CMS.P-NAVSW-20.37.4 CI-TT-PSA.P1CMS.P-NAVSW-20.40.1 CI-TT-PSA.P1CMS.P-NAVSW-20.42.1 CI-TT-PSA.P1CMS.P-NAVSW-20.44.1 CI-TT-PSA.P1CMS.P-NAVSW-21.13.1 CI-TT-PSA.P1CMS.P-NAVSW-21.14.5 CI-TT-PSA.P1CMS.P-NAVSW-21.15.3 CI-TT-PSA.P1CMS.P-NAVSW-21.19.2</tns:navigationSoftwareCompatibilityList>
		<tns:mapCensoringNumber>NotApplicable</tns:mapCensoringNumber>
		<tns:mapPublicationNumber>NotApplicable</tns:mapPublicationNumber>
		<tns:ndsMapLineId>PSA_P1CMS_Japan_3</tns:ndsMapLineId>
		<tns:ndsMapId>41972</tns:ndsMapId>
		<tns:releaseCycle>2020.12</tns:releaseCycle>
		<tns:databaseFormatVersion>SQLite 3.30.0_1</tns:databaseFormatVersion>
		<tns:databaseModelVersion>2.4.6</tns:databaseModelVersion>
		<tns:ndsMapProducts>
			<tns:product baseLineMapId="22252" databaseModelVersion="2.4.6" isComplete="1" ndsDBSupplierId="28" productId="15083325" productStorageSizeInBytes="12992512" productTotalSizeInBytes="12992512" productType="DISPLAY_ONLY WORLD_OVERVIEW" versionId="1"/>
			<tns:product baseLineMapId="21041" databaseModelVersion="2.4.6" isComplete="1" ndsDBSupplierId="30" productId="0" productStorageSizeInBytes="8638312448" productTotalSizeInBytes="8638312448" productType="NAVIGATION POI OBJECTS_3D ADAS" versionId="1"/>
        </tns:ndsMapProducts>      
    </tns:item>
	<tns:region>Japan</tns:region>
	<tns:format>NDS</tns:format>
	<tns:geoCoverage>JPN</tns:geoCoverage>
	<tns:releaseType precondition="CG-TT-PSA.P1CMS.UP-MAP.JAPAN.NDS-2.0.0 CG-TT-PSA.P1CMS.UP-MAP.JAPAN.NDS-1.0.0" predecessor="CG-TT-PSA.P1CMS.UP-MAP.JAPAN.NDS-2.0.0" xsi:type="tns:MapGroupUpdateReleaseType"/>
	<tns:navigationSoftwareCompatibilityList>CG-TT-PSA.P1CMS.P-NAVSW-10.1.7 CG-TT-PSA.P1CMS.P-NAVSW-10.3.6 CG-TT-PSA.P1CMS.P-NAVSW-10.5.15 CG-TT-PSA.P1CMS.P-NAVSW-11.1.9 CG-TT-PSA.P1CMS.P-NAVSW-11.2.13 CG-TT-PSA.P1CMS.P-NAVSW-11.2.49 CG-TT-PSA.P1CMS.P-NAVSW-11.5.13 CG-TT-PSA.P1CMS.P-NAVSW-11.6.43 CG-TT-PSA.P1CMS.P-NAVSW-11.6.8 CG-TT-PSA.P1CMS.P-NAVSW-12.1.8 CG-TT-PSA.P1CMS.P-NAVSW-12.2.40 CG-TT-PSA.P1CMS.P-NAVSW-12.2.6 CG-TT-PSA.P1CMS.P-NAVSW-12.2.79 CG-TT-PSA.P1CMS.P-NAVSW-13.0.16 CG-TT-PSA.P1CMS.P-NAVSW-13.1.13 CG-TT-PSA.P1CMS.P-NAVSW-14.0.15 CG-TT-PSA.P1CMS.P-NAVSW-14.1.12 CG-TT-PSA.P1CMS.P-NAVSW-14.2.4 CG-TT-PSA.P1CMS.P-NAVSW-14.3.14 CG-TT-PSA.P1CMS.P-NAVSW-8.12.19 CG-TT-PSA.P1CMS.P-NAVSW-8.14.18 CG-TT-PSA.P1CMS.P-NAVSW-9.1.11 CG-TT-PSA.P1CMS.P-NAVSW-9.2.20 CG-TT-PSA.P1CMS.P-NAVSW-9.3.19 CG-TT-PSA.P1CMS.P-NAVSW-9.5.22</tns:navigationSoftwareCompatibilityList>
</tns:group>
</tns:metadata>